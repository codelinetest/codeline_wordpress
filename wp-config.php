<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'codeline_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'krRi/&2:dyD=^mO|x7hI7XqZ0E^MGjxFa6KrZ!|27);VYUwKwBK,9L*8VW#0FF<`' );
define( 'SECURE_AUTH_KEY',  'r( @yZpl;6xXYJ<S-+xYfzzig y<Rq^E]^1Q*REcP9#Zzl?&dyK]_)[J1m$V,X8d' );
define( 'LOGGED_IN_KEY',    '`i3+otKMup$RyKf.wl6`7]+{H>=SaaCv;cc|}5<%NhTxjZ42N%Nz>gQu`?k>g#(f' );
define( 'NONCE_KEY',        'lSJ-U,e=i>yn>C37+sgc.iH55){)C7r8*v+7O2Y3TIwQf:2EvZG8CK4RkcPQ}I^+' );
define( 'AUTH_SALT',        '@,IB2Y8 rf+zGlfvE<%Y:>KpQJYx4*5h:6w(a+2 zYbkcuN|oNEP)BU;3-t+F|_4' );
define( 'SECURE_AUTH_SALT', '20/ki&p?2I_ ?)F1M/{,`N+HQ*w`HrBAG=ZB,/jzv^Q2-;14F~b.ot`+7o+YJRN3' );
define( 'LOGGED_IN_SALT',   'C!qJFV3W`/w~H# Fd/J[IscW#C)g{98rUV51BSFI}Ps?q,0NT gs=+TIP&Bv2=>/' );
define( 'NONCE_SALT',       '>Gb0jH+!Ju#qem*5Y8m98%6KO[5mR&+_hvnLbJy*EUL]@H,2SyuVoM^+!F 4ejVc' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'cwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
