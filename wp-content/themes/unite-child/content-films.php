<?php
/**
 * @package unite
 */
$meta = get_post_meta( get_the_ID(), 'addtion_info', true );
?>

<article class="film--holder" id="post-<?php the_ID(); ?>">

	<div class="film film--single">
		<div class="film__data">
			<div class="film__poster">
				<span class="film__poster--fill">
					<img src="<?php echo the_post_thumbnail_url();?>" alt="<?php the_title(); ?>">
				</span>
				<span class="film__poster--featured">
					<a href="<?php the_permalink(); ?>" rel="bookmark">
						<img src="<?php echo the_post_thumbnail_url();?>" alt="<?php the_title(); ?>">
					</a>
				</span>
			</div>
			<div class="film__details">
				<h2 class="film__title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				
				<p class="film__plot">
					<?php the_content(); ?>
						
					</p>
				<ul class="film__tags list--inline mb-1">
					<li>
						<?php echo get_the_term_list( get_the_ID(), 'year_released', 'Year: ', ', ' ); ?>
					</li>
					<li><?php echo get_the_term_list( get_the_ID(), 'genre', 'Genre: ', ', ' ); ?></li>
				</ul>
				<div class="film__credits">
					<p class="mb-0"><strong>Starring:</strong></p>
					<ul class="film__actors list--inline mb-1">
						<li><?php echo get_the_term_list( get_the_ID(), 'actor', '', '<li>' ); ?></li>
					</ul>
				</div>

				<div class="film__credits">
					<div class="row">
						<div class="col-md-6">
							<p class="mb-0"><strong>Country</strong></p>
							<p><?php echo get_the_term_list( get_the_ID(), 'country', '', ', ' ); ?></p>
							
						</div>
						<div class="col-md-6">
							<p class="mb-0"><strong>Release Date</strong></p>
							<p><?php echo date("F j, Y", strtotime($meta['release_date'])); ?></p>
							
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="price-tag">
			<span class="small">Ticket Price</span>
			$<?php echo $meta['ticket_price']; ?>
		</div>
</article>

