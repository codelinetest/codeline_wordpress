<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package unite
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">

		<header id="masthead" class="site-header" role="banner">
			<div class="container header-area">

				<?php do_action( 'before' ); ?>

				<div class="row">
					<div class="site-branding col-md-4">
						<?php

						$custom_logo_id = get_theme_mod( 'custom_logo' );
						if($custom_logo_id):?>

							<?php $logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' ); ?>

							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
								<img src="<?php echo esc_url( $logo_url ); ?>" alt="<?php bloginfo( 'name' ); ?>" width="180">
							</a>

						<?php endif;?>								

						<?php if( !$custom_logo_id ) : ?>
							<h1 class="site-title">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
							</h1>
						<?php endif;?>
					</div>


					<div class="col-md-8">
						<nav class="navbar navbar-default" role="navigation">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>

								</div>

								<?php
								wp_nav_menu( array(
									'theme_location'    => 'primary',
									'depth'             => 2,
									'container'         => 'div',
									'container_class'   => 'collapse navbar-collapse navbar-ex1-collapse',
									'menu_class'        => 'nav navbar-nav',
									'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
									'walker'            => new wp_bootstrap_navwalker())
							);
							?>
					</nav><!-- .site-navigation -->
				</div>


			</div>
			<!-- row end -->
		</div> <!-- container -->
	</header><!-- #masthead -->
	


	<div id="content" class="site-content container"><?php
	global $post;
	if( is_singular() && get_post_meta($post->ID, 'site_layout', true) ){
		$layout_class = get_post_meta($post->ID, 'site_layout', true);
	}
	else{
		$layout_class = of_get_option( 'site_layout' );
	}
	?>
	<div class="row <?php echo $layout_class; ?>">
