<?php 
/**
 * addtional functions and definitions
 *
 * @package unite
 */
// add custom logo support for the theme
include 'funs/custom-logo.php';
// New Post type for Films
include 'funs/post-film-type.php';
// Taxonomies for Film post type
include 'funs/taxonomies-extra.php';
// Custom fields for film post type
include 'funs/custom-fields-extra.php';
// Shortcode for show recent films
include 'funs/films-shortcodes.php';

add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

function add_my_post_types_to_query( $query ) {
  if ( is_home() && $query->is_main_query() )
    $query->set( 'post_type', 'films' );
  return $query;
}



?>