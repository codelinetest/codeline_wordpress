<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package unite
 */
?>
            </div><!-- row -->
	</div><!-- #content -->

	<footer id="footer" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<div class="row">
				<nav role="navigation" class="col-md-12">
					<?php unite_footer_links(); ?>
				</nav>

				<div class="copyright col-md-6">
					
				</div>
			</div>
		</div><!-- .site-info -->
		<hr>
		<small class="text-center" style="display: block">&copy; Film Base - 2019.</small>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>