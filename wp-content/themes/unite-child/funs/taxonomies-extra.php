<?php
// call function for custom taxonomies
add_action( 'init', 'create_extra_taxonomies', 0 );
// custom taxonomies for film post type
function create_extra_taxonomies() {

	//  register taxonomy for Genres
	$labels = array(
		'name'              => _x( 'Genres', 'taxonomy general name' ),
		'singular_name'     => _x( 'Genre', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Genres' ),
		'all_items'         => __( 'All Genres' ),
		'parent_item'       => __( 'Parent Genre' ),
		'parent_item_colon' => __( 'Parent Genre:' ),
		'edit_item'         => __( 'Edit Genre' ),
		'update_item'       => __( 'Update Genre' ),
		'add_new_item'      => __( 'Add New Genre' ),
		'new_item_name'     => __( 'New Genre Name' ),
		'menu_name'         => __( 'Genre' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_in_nav_menus' => true,
		'rewrite'           => array( 'slug' => 'genre' ),
	);

	register_taxonomy( 'genre', 'films', $args );

	//  register taxonomy for Country
	$labels = array(
		'name'              => _x( 'Country', 'taxonomy general name' ),
		'singular_name'     => _x( 'Country', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Country' ),
		'all_items'         => __( 'All Country' ),
		'parent_item'       => __( 'Parent Country' ),
		'parent_item_colon' => __( 'Parent Country:' ),
		'edit_item'         => __( 'Edit Country' ),
		'update_item'       => __( 'Update Country' ),
		'add_new_item'      => __( 'Add New Country' ),
		'new_item_name'     => __( 'New Country Name' ),
		'menu_name'         => __( 'Country' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'country' ),
	);

	register_taxonomy( 'country', 'films', $args );


	//  register taxonomy for Year
	$labels = array(
		'name'              => _x( 'Year', 'taxonomy general name' ),
		'singular_name'     => _x( 'Year', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Year' ),
		'all_items'         => __( 'All Year' ),
		'parent_item'       => __( 'Parent Year' ),
		'parent_item_colon' => __( 'Parent Year:' ),
		'edit_item'         => __( 'Edit Year' ),
		'update_item'       => __( 'Update Year' ),
		'add_new_item'      => __( 'Add New Year' ),
		'new_item_name'     => __( 'New Year Name' ),
		'menu_name'         => __( 'Year' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'year_released' ),
	);

	register_taxonomy( 'year_released', 'films', $args );

	//  register taxonomy for Actors
	$labels = array(
		'name'              => _x( 'Actors', 'taxonomy general name' ),
		'singular_name'     => _x( 'Actor', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Actors' ),
		'all_items'         => __( 'All Actors' ),
		'parent_item'       => __( 'Parent Actor' ),
		'parent_item_colon' => __( 'Parent Actor:' ),
		'edit_item'         => __( 'Edit Actor' ),
		'update_item'       => __( 'Update Actor' ),
		'add_new_item'      => __( 'Add New Actor' ),
		'new_item_name'     => __( 'New Actor Name' ),
		'menu_name'         => __( 'Actors' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'actor' ),
	);

	register_taxonomy( 'actor', 'films', $args );

}

?>