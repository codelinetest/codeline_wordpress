<?php
    // add shortcode for get latest filems
    add_shortcode( 'latest_films', 'display_recent_films' );

    function display_recent_films(){
        $args = array(
            'post_type' => 'films',
            'post_status' => 'publish',
            'posts_per_page' => 5
        );

        $string = '';
        $query = new WP_Query( $args );
        if( $query->have_posts() ){
            $string .= '<ul>';
            while( $query->have_posts() ){
                $query->the_post();

                $image = get_the_post_thumbnail_url();
                $meta = get_post_meta( get_the_ID(), 'addtion_info', true );

                $string .= '<li><div class="media"><div class="media-left">';
                $string .= '<a href="'.get_the_permalink().'">';
                $string .= '<img class="media-object" src="'.$image.'" alt="'.get_the_title().'">';
                $string .= '</div><div class="media-body">';
                $string .= '<h4 class="media-heading">'.get_the_title().'</h4></a>';
                $string .= '<small>'. get_the_term_list( get_the_ID(), 'year_released', 'Year: ', ', ' ) . '</small>';
                $string .= '<p><small>'. get_the_term_list( get_the_ID(), 'genre', 'Genre: ', ', ' ) . '</small></p>';

                $string .= '<a href="'.get_the_permalink().'"><small>Read More</small></a></div></div>';
                $string .= '<div class="price-tag small">$'.$meta['ticket_price'] .'</div>';
                $string .= '</li>';

            }
            $string .= '</ul>';
        }
        wp_reset_postdata();
        return $string;
    }
?>