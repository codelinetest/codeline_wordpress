<?php
// Hooks unite_film_post function on init wordpress.
add_action( 'init', 'unite_film_post');
// Custom post type for manage films
function unite_film_post() {
  // post type labels
  $labels = array(
    'name'                => __( 'Films' ),
    'singular_name'       => __( 'Film'),
    'menu_name'           => __( 'Films'),
    'parent_item_colon'   => __( 'Parent Film'),
    'all_items'           => __( 'All Films'),
    'view_item'           => __( 'View Film'),
    'add_new_item'        => __( 'Add New Film'),
    'add_new'             => __( 'Add New'),
    'edit_item'           => __( 'Edit Film'),
    'update_item'         => __( 'Update Film'),
    'search_items'        => __( 'Search Film')
  );
  // register new post type using theme prefix 
  register_post_type( 'films',
    array(
      'labels' => $labels,
      'public' => true,
      'has_archive' => true,
      // supports
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
      // icon
      'menu_icon' => 'dashicons-format-video',
      // menu position
      'menu_position' => 2,
    )
  );
}
?>