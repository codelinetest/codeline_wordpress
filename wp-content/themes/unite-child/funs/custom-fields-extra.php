<?php
// add meta box for custom fields
add_action( 'add_meta_boxes', 'unite_add_film_info_box' );

function unite_add_film_info_box() {
  add_meta_box( 
    'add_film_info',
    'Additional Film Info', // label
    'unite_show_add_film_info', // form fields
    'films', // for film post type only
    'side', // position
    'low'
  );
}

// display form for Ticket Price and Release Date custom feilds
function unite_show_add_film_info() {
  global $post;
  // get meta using additon_info label
  $meta = get_post_meta( $post->ID, 'addtion_info', true );
  ?>
  <!-- nonce -->
  <input type="hidden" name="meta_box_nonce" value="<?php echo wp_create_nonce( basename(__FILE__) ); ?>">
  <!-- // table holder for fields -->
  <table style="width: 100%">
    <!-- field Ticket Price Label -->
    <tr>
     <td>
        <label for="addtion_info[ticket_price]" style="font-weight: bold">Ticket Price <small>(USD)</small></label>                
      </td>   
    </tr>
  <!-- field Ticket price input -->
    <tr>
     <td>
      <input type="text" min="0.01" step="0.01" name="addtion_info[ticket_price]" 
            id="addtion_info[ticket_price]" class="regular-text currency" 
            value="<?php if (is_array($meta) && isset($meta['ticket_price'])) {   echo $meta['ticket_price']; } ?>" 
            placeholder="Ticket Price" style="width: 200px">
      </td>
    </tr>
    <!-- field Relase date label -->
    <tr>
      <td>
        <label for="addtion_info[release_date]" style="font-weight: bold">Release Date</small></label>                
      </td>
    </tr>
    <!-- field release date input -->
    <tr>
     <td>
       <input type="date" style="width: 200px" name="addtion_info[release_date]" 
              id="addtion_info[release_date]" class="regular-text" 
              value="<?php if (is_array($meta) && isset($meta['release_date'])) {   echo $meta['release_date']; } ?>" 
              placeholder="Release Date">
     </td>
    </tr>
  </table>
<!-- table ends -->

<?php }

// save and edit extra fields data to db
function unite_save_film_info( $post_id ) {   

    // verify nonce
  if ( isset($_POST['meta_box_nonce']) 
    && !wp_verify_nonce( $_POST['meta_box_nonce'], basename(__FILE__) ) ) {
    return $post_id; 
  }
  // check autosave
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return $post_id;
  }


  // check permissions
  if (isset($_POST['post_type'])) { 
    if ( 'page' === $_POST['post_type'] ) {
      if ( !current_user_can( 'edit_page', $post_id ) ) {
        return $post_id;
      } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
      }  
    }
  }

  $old = get_post_meta( $post_id, 'addtion_info', true );
      if (isset($_POST['addtion_info'])) { //Fix 3
        $new = $_POST['addtion_info'];
        if ( $new && $new !== $old ) {
          update_post_meta( $post_id, 'addtion_info', $new );
        } elseif ( '' === $new && $old ) {
          delete_post_meta( $post_id, 'addtion_info', $old );
        }
      }

  }
  // triger save film info when save
  add_action( 'save_post', 'unite_save_film_info' );
  ?>