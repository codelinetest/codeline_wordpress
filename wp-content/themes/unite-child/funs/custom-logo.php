<?php

// enable custom logo
function unite_custom_logo() {
	
	add_theme_support( 'custom-logo', array(		
		'flex-width' => true
	) );

}
add_action( 'after_setup_theme', 'unite_custom_logo' );

function theme_prefix_the_custom_logo() {
	
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}

}

?>